# README #

This README documents the steps that are necessary to get your application up and running.

## Pre-requirements
- Text editor / IDE e.g. Visual Studio Code, PHPStorm
- Git
- HTTP server: `apache`, `nginx`
- PHP version: `>= 5.6` or `>= 7.2`
- Composer
- Npm

#### MacOS
- [Homebrew](https://brew.sh/index_it)
- [Git](https://www.atlassian.com/git/tutorials/install-git#homebrew)
- HTTP server + PHP: `MAMP` (no multiple hosts), `XAMPP`

#### Windows
- HTTP server + PHP: `XAMPP` 

#### Optional
- Git UI e.g. Sourcetree

---

## Repository
- Bitbucket hosts the repository
- [Register an Atlassian account (trello, bitbucket)](https://id.atlassian.com/signup)
- Grant access by adding user to dev group

#### Clone repository
- Execute: `git clone git@bitbucket.org:raneri/diabetescareonline.net.git --branch app-name`
- Set project structure `app-name/app/cms`

#### Install `composer.json` required php extensions
e.g. `php-intl`

#### Install composer required packages
`composer install --no-scripts`

### Environment variables `.env`
- Copy `.env.example`, `cp .env.example .env`
- Configure database environment variables

### Setup front end
- Install `npm`
- `npm install`
- Compile assets `sass`, `javascript`: `npm run dev`
- Watch assets: `npm run watch`

---

## Project information

### Folders

#### Views (Blade templates)
- File extension: `.blade.php`
- Path: `resources/views`

##### Layouts
- Path: `resources/views/layouts`

##### Documentation links
- [Blade](https://laravel.com/docs/5.1/blade)
- [Blade Layout](https://laravel.com/docs/5.1/blade#defining-a-layout)

#### SASS / SCSS
- File extension: `.scss`
- Admin assets: `resources/assets/admin/sass`
- Front assets: `resources/assets/sass`

### Tips
- Update the local repository before committing or pushing commits
- To always see the assets' latest version: enable checkbox `Disable cache` in browser's devtools `Network` tab
